import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './src/app/pages/home/home.component';
import { ToolbarComponent } from './src/app/ui/toolbar/toolbar.component';
import { MainInfoComponent } from './src/app/ui/main-info/main-info.component';
import { ServicesComponent } from './src/app/ui/services/services.component';
import { ExportComponent } from './src/app/ui/export/export.component';
import { PriceComponent } from './src/app/pages/price/price.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    MainInfoComponent,
    ServicesComponent,
    ExportComponent,
    PriceComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
